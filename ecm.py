#!/usr/bin/env python

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.tsa.stattools as ts
import statsmodels.api as sm


class ECM:

    '''simple ECM example
    '''

    def __init__(self, raw):
        self.raw = raw

        self.logrt = np.log(self.raw)

        self._I()


    def _I(self):

        '''I阶单整
        '''
        data = [self.logrt.C, self.logrt.GDP, self.raw.C, self.raw.GDP]

        self.I = [0]*4

        for i, d in enumerate(data):
            self.I[i] = self._unitroot_test(d)


    def _unitroot_test(self, x):

        '''Unit Root Test
        '''
        result = True
        I = 0

        while True:
            result = self._adf(x)

            if not result:
                break

            x = x.diff().dropna()
            I += 1

        return I

    def _adf(self, x, crit='5%', maxlag=6, regression='nc', autolag='AIC'):

        '''ADF test
        '''
        boolean = False

        adf = ts.adfuller(x, maxlag=maxlag)

        if adf[0]>=adf[4][crit]:
            boolean = True

        return boolean


    def plot_data(self):

        '''原始数据和对数收益率
        '''
        fig, axes = plt.subplots(4, 1)

        self.raw.C.plot(ax=axes[0], title='C')
        self.logrt.C.plot(ax=axes[1], title='ln(C)')
        self.raw.GDP.plot(ax=axes[2], title='GDP')
        self.logrt.GDP.plot(axes=axes[3], title='ln(GDP)')

        plt.show()


    def plot_acf(self):

        '''原始数据和对数收益率ACF
        '''
        data = self.logrt
        diff = data.diff().dropna()

        fig, axes = plt.subplots(2, 2)

        sm.graphics.tsa.plot_acf(data.C, ax=axes[0,0])
        sm.graphics.tsa.plot_acf(diff.C, ax=axes[0,1])

        sm.graphics.tsa.plot_acf(data.GDP, ax=axes[1,0])
        sm.graphics.tsa.plot_acf(diff.GDP, ax=axes[1,1])

        plt.show()


    def cointegration(self, logrt=False, get_ecm=False):

        '''EG两步协整检验
        '''
        if logrt:
            data = self.logrt
        else:
            data = self.raw

        Y = data.C[1:]

        # 因简单回归模型残差有较强一阶自相关性，因此适当加入滞后项
        lagData = data.shift(1)
        df = pd.concat([data.GDP, lagData.C, lagData.GDP], axis=1,
                       keys=['GDP','rC','rGDP']).dropna()

        X = sm.add_constant(df)

        model = sm.OLS(Y, X)
        results = model.fit()

        self.resid = results.resid
        self.params = results.params

        if get_ecm:
            self._ecm(data)
        else:
            self.results = results

            # 确定ADF模型和判断协整
            # print(self.resid.mean())

            I = self._unitroot_test(self.resid)
            # print('残差resid~I(' + str(I) + ')')


    def _ecm(self, data):

        '''ECM
        '''
        diff = data.diff().dropna()
        Y = diff.C[1:]

        lagData = diff.shift(1)
        lagResid = self.resid.shift(1)
        X = pd.concat([diff.GDP, lagData.C, lagData.GDP, lagResid], axis=1,
                      keys=['dGDP','drC','drGDP','Resid']).dropna()

        model = sm.OLS(Y, X)

        self.results = model.fit()


    def summary(self):

        '''OLS summary
        '''
        print(self.results.summary())


    def plot(self):

        '''绘制残差图
        '''
        self.resid.plot()
        plt.show()


if __name__ == '__main__':

    fname = 'data.csv'
    path = os.path.join('data', os.path.basename(fname))

    logrt = True
    get_ecm = True

    try:
        data = pd.read_csv(path, index_col='Year')

        ecm = ECM(data)

        # print(ecm.I)

        # ecm.plot_data()
        # ecm.plot_acf()
        ecm.cointegration(logrt, get_ecm)

        # ecm.plot()
        ecm.summary()

    except KeyboardInterrupt as err:
        print('Shut down!')